# Fan Control

Python script to simultaniously control all computer fans based on the worst temperature in the system.

I wrote this script because every other program was only capable of binding a specific fan to a specific temperature.
This script looks for the worst temperature, calculates how bad it is, and applies a fan speed based on it. The same speed is applied to all fans that can be controlled with pwm. If the temperature rises, fans spin up fast, but if the temperature drops, the fan speed only drops slowly. This avoids that the fans ramp up and down all the time.

The speed is calculated by a polynomial, so it's smooth and the fans should be low if temperatures are good. You can change the function yourself if it's not to your liking.

This script uses the `sensors` command. You should run `sensors-detect` before running the script: https://wiki.archlinux.org/title/Fan_speed_control#Fancontrol_(lm-sensors)

## Settings

You can configure several variables. To do this, you need to edit the variables at the beginning directly in the script.

You can edit:
- The default temperature limit (default 70°C)
- Custom limits for special devices. The device names are the same names that you can see when typing `sensors` in the terminal.
- The courve function
- The number of last temperatures that are to be saved for smoother fan speed reduction

## Disclamer

This is only tested on my personal system. It may inflict any type of damage, and harm you, your computer, or your loved ones. Use at your own risk!

## Usage
To start it, run `sudo python3 /path/to/fan_control.py`

You may also autostart it with cron or something similar, for example with `sudo crontab -e`, and then add the line `@reboot /path/to/fan-control/fan_control.py --quiet`.

## License

GPL version 3
