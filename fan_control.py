#!/usr/bin/python3

#    Copyright (C) 2021  Waldelb
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


################################################################################################################################################

# You can edit the following variables

# Default temperature limit that is applied to all devices that are not listed in custom:
default_limit = 70

# Custom temperature limit for following devices:
custom = {
    "CPU": 70,
    "System": 70,
    "Sensor 2": 80,
    "mem": 85,
    "junction": 80,
    "edge": 80,
}

# Curve function that is used to calculate the fan speed:
f = (lambda x: (x-0.04)**7+0.2)

# Number of saved temperatures for smoother speed reduction:
blen = 10

################################################################################################################################################

import subprocess
import time
import re
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-q", "--quiet", help="Print no information", action='store_false')
args = parser.parse_args()
print_output = args.quiet

# Find path to pwm files.
pwm_files = []
for root, dirs, files in os.walk("/sys/devices/platform"):
    if "pwm1" in files and "hwmon" in root:
        if oct(os.stat(f"{root}/pwm1").st_mode)[-3] != "4":
            pwm_files = [f"{root}/{x}" for x in files if "pwm" in x]


if not pwm_files:
    print(
        "There were no writable pwm files found. You either need to execute the script as root, or, if you already did that, maybe install a kernel module.")
    exit(0)


# init ringbuffer for softer slowdown
last_levels = [80] * blen
pos = 0

# loop forever
while True:
    # get new temperatures
    out = subprocess.check_output("sensors", stderr=subprocess.DEVNULL).decode("utf-8").split('\n')

    temps = {
        line[0]: float(line[1].strip(" +")) for line in
        [re.sub("°C.*", "", line).split(":") for line in
         [re.sub("\(.*", "", line) for line in out]
         if "°C" in line]
    }

    # calculate how bad temperatures are
    output = {}
    max_device = ""
    max_len = 0
    max_factor = 0

    for device, temp in temps.items():
        current_limit = custom[device] if device in custom else default_limit
        current_factor = min(f(temp / current_limit), 1)

        if print_output:
            # calculate stuff for output
            output[device] = [device, str(temp), str(int(current_factor * 100))]
            max_len = max(len(device), max_len)

        if current_factor > max_factor:
            max_factor = current_factor
            max_device = device

    # new level for all fans. 255 is the maximum
    new_level = 255 * max_factor

    # slow down slowly
    last_levels[pos] = new_level
    avg = sum(last_levels) / blen
    new_level = str(int(max(new_level, avg)))
    
    pos = (pos + 1) % blen

    if print_output:
        os.system("clear")
        print("\n".join(
            (lambda x: x if device != max_device else f"\033[0;31m{x}\033[0;0m")(
                f"{x[0].ljust(max_len)}  {x[1].rjust(5)}°C {x[2].rjust(5)}%")
            for device, x in output.items()
        ) + f"\n -> {new_level.rjust(3)} / 255")
    
    # Apply new pwm level for all fans
    for file_name in pwm_files:
        with open(file_name, "w") as file:
            file.write(new_level)
    time.sleep(1)
